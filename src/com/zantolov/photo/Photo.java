package com.zantolov.photo;

public class Photo {

    private double aperture;

    private double shutterSpeed;

    public Photo(double aperture, double shutterSpeed) {
        this.aperture = aperture;
        this.shutterSpeed = shutterSpeed;
    }

    public double getAperture() {
        return aperture;
    }

    public void setAperture(double aperture) {
        this.aperture = aperture;
    }

    public double getShutterSpeed() {
        return shutterSpeed;
    }

    public void setShutterSpeed(double shutterSpeed) {
        this.shutterSpeed = shutterSpeed;
    }
}
