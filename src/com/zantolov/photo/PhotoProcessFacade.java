package com.zantolov.photo;

public class PhotoProcessFacade {

    public void processPhoto(Photo photo) {
        this.fixBrightness(photo);
        this.crop(photo);
        this.fixContrast(photo);
    }

    private void fixBrightness(Photo photo) {
        if (photo.getShutterSpeed() > 5) {
            photo.setShutterSpeed(photo.getShutterSpeed() - 1);
        }
    }

    private void fixContrast(Photo photo) {
        if (photo.getAperture() > 5) {
            photo.setAperture(photo.getAperture() - 1);
        }
    }

    private void crop(Photo photo) {
        // dummy method
    }

}
