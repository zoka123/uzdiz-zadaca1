package com.zantolov.registry;

public class RegistryBuilder {

    private static void buildRegistry(String[] args, Registry registry) {
        /**
         1 sjeme za generator slučajnog broja (min 3 znamenke),
         2 maksimalni broj tema,
         3 maksimalni broj tema po natjecatelju,
         4 maksimalni broj kategorija po natjecatelju,
         5 maksimalni broj natjecatelja,
         6 broj članova žiria,
         7 naziv klase za algoritam bodovanja,
         8 naziv datoteke
         */

        if (Integer.valueOf(args[3]) > 3) {
            throw new IllegalArgumentException("Broj kategorija ne smije biti veci od 3");
        }

        if (Integer.valueOf(args[1]) > 10) {
            throw new IllegalArgumentException("Broj tema ne smije biti veći od 1");
        }

        if (Integer.valueOf(args[2]) > 3) {
            throw new IllegalArgumentException("Broj kategorija po natjecatelju");
        }

        // Set arguments in registry
        registry.setRandomNumberGeneratorSeed(Long.valueOf(args[0]));
        registry.setMaxTopicNumber(Integer.valueOf(args[1]));
        registry.setMaxTopicPerCompetitor(Integer.valueOf(args[2]));
        registry.setMaxCategoryPerCompetitor(Integer.valueOf(args[3]));
        registry.setMaxCompetitorNumber(Integer.valueOf(args[4]));
        registry.setMaxJuryMembersNumber(Integer.valueOf(args[5]));
        registry.setPointsResolverClassName(args[6]);
        registry.setOutputFilename(args[7]);
    }

    public static Registry getRegistry(String[] args) {
        Registry registry = new Registry();
        buildRegistry(args, registry);
        return registry;
    }

}
