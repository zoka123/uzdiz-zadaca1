package com.zantolov.registry;

import com.zantolov.camera.RandomCameraGenerator;
import com.zantolov.camera.builder.CameraBuilderFactory;
import com.zantolov.contest.score.*;
import com.zantolov.contest.topic.ContestTopicMapBuilder;
import com.zantolov.random.RandomNumberGenerator;

public class Registry {

    private RandomNumberGenerator generator = null;
    private long randomNumberGeneratorSeed;

    private int maxTopicNumber;
    private int maxTopicPerCompetitor;
    private int maxCategoryPerCompetitor;
    private int maxCompetitorNumber;
    private int maxJuryMembersNumber;
    private String pointsResolverClassName;
    private String outputFilename;
    private ContestTopicMapBuilder contestTopicMapBuilder = null;
    private RandomCameraGenerator cameraGenerator = null;
    private CameraBuilderFactory cameraBuilderFactory = null;
    private ScoreResolverInterface scoreResolver = null;
    private ScoreSummaryGenerator scoreSummaryGenerator = null;


    public ContestTopicMapBuilder getContestTopicMapBuilder() {
        if (this.contestTopicMapBuilder == null) {
            this.contestTopicMapBuilder = new ContestTopicMapBuilder(this.getGenerator());
        }

        return this.contestTopicMapBuilder;
    }

    public int getMaxTopicNumber() {
        return maxTopicNumber;
    }

    public void setMaxTopicNumber(int maxTopicNumber) {
        this.maxTopicNumber = maxTopicNumber;
    }

    public int getMaxTopicPerCompetitor() {
        return maxTopicPerCompetitor;
    }

    public void setMaxTopicPerCompetitor(int maxTopicPerCompetitor) {
        this.maxTopicPerCompetitor = maxTopicPerCompetitor;
    }

    public int getMaxCategoryPerCompetitor() {
        return maxCategoryPerCompetitor;
    }

    public void setMaxCategoryPerCompetitor(int maxCategoryPerCompetitor) {
        this.maxCategoryPerCompetitor = maxCategoryPerCompetitor;
    }

    public int getMaxCompetitorNumber() {
        return maxCompetitorNumber;
    }

    public void setMaxCompetitorNumber(int maxCompetitorNumber) {
        this.maxCompetitorNumber = maxCompetitorNumber;
    }

    public int getMaxJuryMembersNumber() {
        return maxJuryMembersNumber;
    }

    public void setMaxJuryMembersNumber(int maxJuryMembersNumber) {
        this.maxJuryMembersNumber = maxJuryMembersNumber;
    }

    public String getPointsResolverClassName() {
        return pointsResolverClassName;
    }

    public void setPointsResolverClassName(String pointsResolverClassName) {
        this.pointsResolverClassName = pointsResolverClassName;
    }

    public String getOutputFilename() {
        return outputFilename;
    }

    public void setOutputFilename(String outputFilename) {
        this.outputFilename = outputFilename;
    }

    public RandomNumberGenerator getGenerator() {
        if (this.generator == null) {
            this.generator = new RandomNumberGenerator(this.getRandomNumberGeneratorSeed());
        }
        return this.generator;
    }

    public void setGenerator(RandomNumberGenerator generator) {
        this.generator = generator;
    }

    public long getRandomNumberGeneratorSeed() {
        return this.randomNumberGeneratorSeed;
    }

    public void setRandomNumberGeneratorSeed(long randomNumberGeneratorSeed) {
        this.randomNumberGeneratorSeed = randomNumberGeneratorSeed;
    }

    public CameraBuilderFactory getCameraBuilderFactory() {

        if (this.cameraBuilderFactory == null) {
            this.cameraBuilderFactory = new CameraBuilderFactory();
        }
        return this.cameraBuilderFactory;
    }

    public RandomCameraGenerator getCameraGenerator() {
        if (this.cameraGenerator == null) {
            this.cameraGenerator = new RandomCameraGenerator(this.getGenerator(), this.getCameraBuilderFactory());
        }

        return this.cameraGenerator;
    }

    public ScoreResolverInterface getScoreResolver() {
        if (this.scoreResolver == null) {
            switch (this.getPointsResolverClassName()) {
                case "AllPointsScoreResolver":
                    this.scoreResolver = new AllPointsScoreResolver(this.getGenerator());
                    break;
                case "ExceptMaxMinPointsScoreResolver":
                    this.scoreResolver = new ExceptMaxMinPointsScoreResolver(this.getGenerator());
                    break;
                case "MaxMinAveragePointsScoreResolver":
                    this.scoreResolver = new MaxMinAveragePointsScoreResolver(this.getGenerator());
                    break;
                default:
                    return null;
            }
        }

        return this.scoreResolver;
    }

    public ScoreSummaryGenerator getScoreSummaryGenerator() {
        if (this.scoreSummaryGenerator == null) {
            this.scoreSummaryGenerator = new ScoreSummaryGenerator();
        }

        return scoreSummaryGenerator;
    }
}
