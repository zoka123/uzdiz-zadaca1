package com.zantolov;

import com.zantolov.contest.ContestFacade;
import com.zantolov.registry.Registry;
import com.zantolov.registry.RegistryBuilder;

public class Main {

    public static void main(String[] args) {

        Registry registry = RegistryBuilder.getRegistry(args);

        ContestFacade contestFacade = ContestFacade.getInstance();
        contestFacade.setRegistry(registry);
        contestFacade.run();

    }
}
