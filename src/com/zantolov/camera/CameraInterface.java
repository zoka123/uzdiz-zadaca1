package com.zantolov.camera;

public interface CameraInterface {

    public String getName();

    public String getLens();

    public String getType();

}
