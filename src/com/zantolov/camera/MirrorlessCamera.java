package com.zantolov.camera;

public class MirrorlessCamera extends Camera {

    private double zoomFactor;

    private boolean hasAdditionalGrip;

    public String getType() {
        return "MIRRORLESS";
    }

    public double getZoomFactor() {
        return zoomFactor;
    }

    public void setZoomFactor(double zoomFactor) {
        this.zoomFactor = zoomFactor;
    }

    public boolean isHasAdditionalGrip() {
        return hasAdditionalGrip;
    }

    public void setHasAdditionalGrip(boolean hasAdditionalGrip) {
        this.hasAdditionalGrip = hasAdditionalGrip;
    }
}
