package com.zantolov.camera;

public class DSLRCamera extends Camera {

    private double zoomFactor;

    private boolean hasGrip;

    private boolean hasFlash;

    private boolean hasInCameraStabilizer;

    public String getType() {
        return "DSLR";
    }


    public double getZoomFactor() {
        return zoomFactor;
    }

    public void setZoomFactor(double zoomFactor) {
        this.zoomFactor = zoomFactor;
    }

    public boolean isHasGrip() {
        return hasGrip;
    }

    public void setHasGrip(boolean hasGrip) {
        this.hasGrip = hasGrip;
    }

    public boolean isHasFlash() {
        return hasFlash;
    }

    public void setHasFlash(boolean hasFlash) {
        this.hasFlash = hasFlash;
    }

    public boolean isHasInCameraStabilizer() {
        return hasInCameraStabilizer;
    }

    public void setHasInCameraStabilizer(boolean hasInCameraStabilizer) {
        this.hasInCameraStabilizer = hasInCameraStabilizer;
    }
}
