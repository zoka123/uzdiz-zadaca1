package com.zantolov.camera.builder;

public class ConcreteMirrorlessFujiXT10Builder extends AbstractMirrorlessBuilder {

    @Override
    protected void buildCameraName() {
        this.getCamera().setName("Fuji X-T10");
    }

    @Override
    protected void buildLens() {
        this.getCamera().setLens("18-55mm Lens");
    }

    @Override
    protected void buildZoomFactor() {
        this.getCamera().setZoomFactor(5.3);
    }

    @Override
    protected void buildHasAdditionalGrip() {
        this.getCamera().setHasAdditionalGrip(false);
    }
}
