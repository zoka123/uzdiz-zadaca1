package com.zantolov.camera.builder;

public class ConcreteDSLRPentaxK500Builder extends AbstractDSLRBuilder {

    @Override
    public void buildCameraName() {
        this.getCamera().setName("Pentax K-500");
    }

    @Override
    public void buildLens() {
        this.getCamera().setLens("Pentax smc DA 50mm f/1.8 Lens");
    }

    @Override
    public void buildZoomFactor() {
        this.getCamera().setLens("1.8");
    }

    @Override
    public void buildHasGrip() {
        this.getCamera().setHasGrip(false);
    }

    @Override
    public void buildHasFlash() {
        this.getCamera().setHasFlash(true);
    }

    @Override
    public void buildHasInCameraStabilizer() {
        this.getCamera().setHasInCameraStabilizer(true);
    }

}
