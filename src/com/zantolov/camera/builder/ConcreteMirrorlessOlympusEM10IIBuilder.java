package com.zantolov.camera.builder;

public class ConcreteMirrorlessOlympusEM10IIBuilder extends AbstractMirrorlessBuilder {

    @Override
    protected void buildCameraName() {
        this.getCamera().setName("Olympus OM-D");
    }

    @Override
    protected void buildLens() {
        this.getCamera().setLens("M.Zuiko ED 7-14mm f2.8 PRO");
    }

    @Override
    protected void buildZoomFactor() {
        this.getCamera().setZoomFactor(4.1);
    }

    @Override
    protected void buildHasAdditionalGrip() {
        this.getCamera().setHasAdditionalGrip(false);
    }
}
