package com.zantolov.camera.builder;

import com.zantolov.camera.CompactCamera;

abstract public class AbstractCompactBuilder extends AbstractCameraBuilder {

    CompactCamera camera = new CompactCamera();

    protected final void buildZoomFactor() {
    }

    protected final void buildHasGrip() {
    }

    protected final void buildHasFlash() {
    }

    protected final void buildHasInCameraStabilizer() {
    }

    protected final void buildHasAdditionalGrip() {
    }

    public CompactCamera getCamera() {
        return this.camera;
    }
}
