package com.zantolov.camera.builder;

public class ConcreteDSLRCanonEOS100Builder extends AbstractDSLRBuilder {

    @Override
    public void buildCameraName() {
        this.getCamera().setName("Canon EOS 100D");
    }

    @Override
    public void buildLens() {
        this.getCamera().setLens("Canon EF 50mm f/1.8 STM Lens");
    }

    @Override
    public void buildZoomFactor() {
        this.getCamera().setLens("1.8");
    }

    @Override
    public void buildHasGrip() {
        this.getCamera().setHasGrip(true);
    }

    @Override
    public void buildHasFlash() {
        this.getCamera().setHasFlash(false);
    }

    @Override
    public void buildHasInCameraStabilizer() {
        this.getCamera().setHasInCameraStabilizer(true);
    }

}
