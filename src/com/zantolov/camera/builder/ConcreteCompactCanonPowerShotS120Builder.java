package com.zantolov.camera.builder;

public class ConcreteCompactCanonPowerShotS120Builder extends AbstractCompactBuilder {

    @Override
    public void buildCameraName() {
        this.getCamera().setName("Canon PowerShot S120");
    }

    @Override
    public void buildLens() {
        this.getCamera().setLens("24-120mm");
    }

    @Override
    protected void buildOcular() {
        this.getCamera().setOcular(false);
    }
}
