package com.zantolov.camera.builder;

import com.zantolov.camera.CompactCamera;
import com.zantolov.camera.MirrorlessCamera;

abstract public class AbstractMirrorlessBuilder extends AbstractCameraBuilder {

    MirrorlessCamera camera = new MirrorlessCamera();

    protected final void buildHasGrip() {
    }

    protected final void buildHasFlash() {
    }

    protected final void buildHasInCameraStabilizer() {
    }

    protected final void buildOcular() {
    }

    public MirrorlessCamera getCamera() {
        return this.camera;
    }
}
