package com.zantolov.camera.builder;

public class ConcreteMirrorlessSonyA7IIBuilder extends AbstractMirrorlessBuilder {

    @Override
    protected void buildCameraName() {
        this.getCamera().setName("Sony A7 II");
    }

    @Override
    protected void buildLens() {
        this.getCamera().setLens("Sony FE 16-35mm f/4 ZA OSS Lens");
    }

    @Override
    protected void buildZoomFactor() {
        this.getCamera().setZoomFactor(6.3);
    }

    @Override
    protected void buildHasAdditionalGrip() {
        this.getCamera().setHasAdditionalGrip(true);
    }
}
