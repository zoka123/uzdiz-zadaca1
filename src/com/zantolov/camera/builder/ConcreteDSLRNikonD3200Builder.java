package com.zantolov.camera.builder;

public class ConcreteDSLRNikonD3200Builder extends AbstractDSLRBuilder {

    @Override
    public void buildCameraName() {
        this.getCamera().setName("Nikon D3200");
    }

    @Override
    public void buildLens() {
        this.getCamera().setLens("Nikon AF-S NIKKOR 85mm f/1.4G Lens");
    }

    @Override
    public void buildZoomFactor() {
        this.getCamera().setLens("1.8");
    }

    @Override
    public void buildHasGrip() {
        this.getCamera().setHasGrip(false);
    }

    @Override
    public void buildHasFlash() {
        this.getCamera().setHasFlash(true);
    }

    @Override
    public void buildHasInCameraStabilizer() {
        this.getCamera().setHasInCameraStabilizer(true);
    }

}
