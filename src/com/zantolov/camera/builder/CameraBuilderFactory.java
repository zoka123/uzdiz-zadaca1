package com.zantolov.camera.builder;

public class CameraBuilderFactory {

    public AbstractCameraBuilder make(String type) {
        AbstractCameraBuilder builder;

        switch (type) {
            case "PentaxK500":
                builder = new ConcreteDSLRPentaxK500Builder();
                break;
            case "NikonD3200":
                builder = new ConcreteDSLRNikonD3200Builder();
                break;
            case "CanonEOS100":
                builder = new ConcreteDSLRCanonEOS100Builder();
                break;
            case "OlympusEM10II":
                builder = new ConcreteMirrorlessOlympusEM10IIBuilder();
                break;
            case "SonyA7II":
                builder = new ConcreteMirrorlessSonyA7IIBuilder();
                break;
            case "FujiXT10":
                builder = new ConcreteMirrorlessSonyA7IIBuilder();
                break;
            case "CanonPowerShotS120":
                builder = new ConcreteCompactCanonPowerShotS120Builder();
                break;
            case "PanasonicLumixTZ70":
                builder = new ConcreteCompactPanasonicLumixTZ70Builder();
                break;
            case "SonyCyberShotRX100":
                builder = new ConcreteCompactSonyCyberShotRX100Builder();
                break;
            default:
                throw new IllegalArgumentException();
        }

        return builder;
    }

}
