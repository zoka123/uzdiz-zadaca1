package com.zantolov.camera.builder;

public class ConcreteCompactPanasonicLumixTZ70Builder extends AbstractCompactBuilder {

    @Override
    public void buildCameraName() {
        this.getCamera().setName("Panasonic Lumix TZ70");
    }

    @Override
    public void buildLens() {
        this.getCamera().setLens("30x Zoom LEICA Lens");
    }

    @Override
    protected void buildOcular() {
        this.getCamera().setOcular(true);
    }
}
