package com.zantolov.camera.builder;

import com.zantolov.camera.DSLRCamera;

abstract public class AbstractDSLRBuilder extends AbstractCameraBuilder {

    DSLRCamera camera = new DSLRCamera();

    protected final void buildOcular() {
    }

    protected final void buildHasAdditionalGrip() {
    }

    public DSLRCamera getCamera() {
        return this.camera;
    }
}
