package com.zantolov.camera.builder;

import com.zantolov.camera.Camera;

abstract public class AbstractCameraBuilder {

    public void buildCamera() {
        this.buildCameraName();
        this.buildLens();
        this.buildZoomFactor();
        this.buildHasGrip();
        this.buildHasFlash();
        this.buildHasInCameraStabilizer();
        this.buildOcular();
        this.buildHasAdditionalGrip();
    }

    abstract protected void buildCameraName();

    abstract protected void buildLens();

    abstract protected void buildZoomFactor();

    abstract protected void buildHasGrip();

    abstract protected void buildHasFlash();

    abstract protected void buildHasInCameraStabilizer();

    abstract protected void buildOcular();

    abstract protected void buildHasAdditionalGrip();

    abstract public Camera getCamera();
}
