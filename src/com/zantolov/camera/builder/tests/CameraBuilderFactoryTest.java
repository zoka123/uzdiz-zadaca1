package com.zantolov.camera.builder.tests;

import com.zantolov.camera.CompactCamera;
import com.zantolov.camera.DSLRCamera;
import com.zantolov.camera.MirrorlessCamera;
import com.zantolov.camera.builder.AbstractCompactBuilder;
import com.zantolov.camera.builder.AbstractDSLRBuilder;
import com.zantolov.camera.builder.AbstractMirrorlessBuilder;
import com.zantolov.camera.builder.CameraBuilderFactory;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertThat;

public class CameraBuilderFactoryTest {

    @Test
    public void testMakeDslr() throws Exception {
        List<String> dslrCameras = Arrays.asList("PentaxK500", "NikonD3200", "CanonEOS100");

        CameraBuilderFactory factory = new CameraBuilderFactory();
        for (String cameraType : dslrCameras) {
            AbstractDSLRBuilder builder = (AbstractDSLRBuilder) factory.make(cameraType);
            assertThat(builder, Matchers.instanceOf(AbstractDSLRBuilder.class));

            builder.buildCamera();
            DSLRCamera camera = builder.getCamera();
            assertThat(camera, Matchers.instanceOf(DSLRCamera.class));
        }
    }

    @Test
    public void testMakeMirrorless() throws Exception {
        List<String> cameraTypes = Arrays.asList("OlympusEM10II", "SonyA7II", "FujiXT10");
        CameraBuilderFactory factory = new CameraBuilderFactory();

        for (String cameraType : cameraTypes) {
            AbstractMirrorlessBuilder builder = (AbstractMirrorlessBuilder) factory.make(cameraType);
            assertThat(builder, Matchers.instanceOf(AbstractMirrorlessBuilder.class));

            builder.buildCamera();
            MirrorlessCamera camera = builder.getCamera();
            assertThat(camera, Matchers.instanceOf(MirrorlessCamera.class));
        }
    }

    @Test
    public void testMakeCompact() throws Exception {
        List<String> cameraTypes = Arrays.asList("CanonPowerShotS120", "PanasonicLumixTZ70", "SonyCyberShotRX100");
        CameraBuilderFactory factory = new CameraBuilderFactory();

        for (String cameraType : cameraTypes) {
            AbstractCompactBuilder builder = (AbstractCompactBuilder) factory.make(cameraType);
            assertThat(builder, Matchers.instanceOf(AbstractCompactBuilder.class));

            builder.buildCamera();
            CompactCamera camera = builder.getCamera();
            assertThat(camera, Matchers.instanceOf(CompactCamera.class));
        }
    }

}