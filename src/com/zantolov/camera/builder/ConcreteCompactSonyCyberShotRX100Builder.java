package com.zantolov.camera.builder;

public class ConcreteCompactSonyCyberShotRX100Builder extends AbstractCompactBuilder {

    @Override
    public void buildCameraName() {
        this.getCamera().setName("Sony Cyber-shot RX100");
    }

    @Override
    public void buildLens() {
        this.getCamera().setLens("10.4-37.1mm");
    }

    @Override
    protected void buildOcular() {
        this.getCamera().setOcular(false);
    }
}
