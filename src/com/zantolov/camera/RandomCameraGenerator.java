package com.zantolov.camera;

import com.zantolov.camera.builder.AbstractCameraBuilder;
import com.zantolov.camera.builder.CameraBuilderFactory;
import com.zantolov.random.RandomNumberGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RandomCameraGenerator {

    private RandomNumberGenerator generator;

    private CameraBuilderFactory builderFactory;

    public RandomCameraGenerator(RandomNumberGenerator generator, CameraBuilderFactory builderFactory) {
        this.generator = generator;
        this.builderFactory = builderFactory;
    }

    public CameraInterface getRandomCamera() {
        List<String> cameraTypes = Arrays.asList("PentaxK500", "NikonD3200", "CanonEOS100", "OlympusEM10II", "SonyA7II", "FujiXT10", "CanonPowerShotS120", "PanasonicLumixTZ70", "SonyCyberShotRX100");
        String randomCamera = cameraTypes.get(this.generator.getRandomInt() % cameraTypes.size());
        AbstractCameraBuilder builder = this.builderFactory.make(randomCamera);
        builder.buildCamera();
        return builder.getCamera();

    }

}
