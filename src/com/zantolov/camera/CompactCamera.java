package com.zantolov.camera;

public class CompactCamera extends Camera {

    private boolean ocular;

    public boolean getOcular() {
        return ocular;
    }

    public void setOcular(boolean ocular) {
        this.ocular = ocular;
    }

    public String getType() {
        return "COMPACT";
    }
}
