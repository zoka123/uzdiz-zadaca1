package com.zantolov.camera;

abstract public class Camera implements CameraInterface {

    private String name;

    private String lens;

    private String type;

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getLens() {

        return lens;
    }

    public void setLens(String lens) {

        this.lens = lens;
    }
}

