package com.zantolov.contest.score;

import com.zantolov.contest.ContestApplicationScore;

import java.util.HashMap;

public interface ScoreResolverInterface {

    public double calculateFinalScore(HashMap<Integer, Integer> scores);

    public void setScoreCorrectness(ContestApplicationScore score);

}
