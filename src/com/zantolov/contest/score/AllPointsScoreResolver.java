package com.zantolov.contest.score;

import com.zantolov.contest.ContestApplication;
import com.zantolov.random.RandomNumberGenerator;

import java.util.HashMap;

public class AllPointsScoreResolver extends AbstractScoreResolver {

    public AllPointsScoreResolver(RandomNumberGenerator randomNumberGenerator) {
        super(randomNumberGenerator);
    }

    @Override
    public double calculateFinalScore(HashMap<Integer, Integer> scores) {
        double sum = 0;

        for (int i = 1; i <= scores.size(); i++) {
            sum += scores.get(i);
        }
        sum = sum / scores.size();
        return sum;
    }

}
