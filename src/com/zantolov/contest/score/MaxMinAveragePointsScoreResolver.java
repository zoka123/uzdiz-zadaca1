package com.zantolov.contest.score;

import com.zantolov.random.RandomNumberGenerator;

import java.util.*;

public class MaxMinAveragePointsScoreResolver extends AbstractScoreResolver {

    public MaxMinAveragePointsScoreResolver(RandomNumberGenerator randomNumberGenerator) {
        super(randomNumberGenerator);
    }

    @Override
    public double calculateFinalScore(HashMap<Integer, Integer> scores) {
        double sum = 0;
        ArrayList<Integer> scoreValues = new ArrayList<>();
        Iterator it = scores.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            scoreValues.add((Integer) pair.getValue());
        }
        Integer max = Collections.max(scoreValues);
        Integer min = Collections.min(scoreValues);

        return (double) (max + min) / 2;
    }

}
