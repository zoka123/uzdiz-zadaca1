package com.zantolov.contest.score;

import com.zantolov.contest.ContestApplicationScore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ScoreSummaryConsoleAdapter {

    private ScoreSummaryGenerator scoreSummary;

    public ScoreSummaryGenerator getScoreSummary() {
        return scoreSummary;
    }

    public ScoreSummaryConsoleAdapter(ScoreSummaryGenerator scoreSummary) {
        this.scoreSummary = scoreSummary;
    }

    public void setScoreSummary(ScoreSummaryGenerator scoreSummary) {
        this.scoreSummary = scoreSummary;
    }

    public String printScoreSummary() {
        String data = "";
        data += this.printTotalScore();
        data += this.printScorePerCategory();
        data += this.printScorePerTopicCategory();
        return data;
    }

    protected String printTotalScore() {
        String data = "TOTAL SCORES:\n=============\n";

        int currentPlace = 0;
        int count = 0;
        double previousScore = 0;
        for (ContestApplicationScore score : this.getScoreSummary().getScores()) {
            count++;
            if (previousScore != score.getFinalScore()) {
                currentPlace++;
            }

            data += currentPlace + ". mjesto:" + score.toString() + "\n";
            previousScore = score.getFinalScore();

            if (count >= 3) {
                break;
            }
        }
        return data;
    }

    protected String printScorePerCategory() {
        String data = "\n\nSCORES BY CATEGORY:\n===================\n";

        HashMap<String, ArrayList<ContestApplicationScore>> categorySummary = this.scoreSummary.getSummary(ScoreSummaryGenerator.SUMMARY_TYPE_CATEGORY);

        Iterator it = categorySummary.entrySet().iterator();
        while (it.hasNext()) {
            int currentPlace = 0;
            int count = 0;
            double previousScore = 0;

            Map.Entry pair = (Map.Entry) it.next();

            data += "\n" + pair.getKey() + ":\n===================\n";

            for (ContestApplicationScore score : (ArrayList<ContestApplicationScore>) pair.getValue()) {
                if (previousScore != score.getFinalScore()) {
                    currentPlace++;
                }

                data += currentPlace + ". mjesto:" + score.toString() + "\n";
                previousScore = score.getFinalScore();

                if (++count >= 3) {
                    break;
                }
            }
        }
        return data;
    }

    protected String printScorePerTopicCategory() {
        String data = "\n\nSCORES BY TOPIC IN CATEGORY:\n================================\n";

        HashMap<String, ArrayList<ContestApplicationScore>> categoryTopicsSummary = this.scoreSummary.getSummary(ScoreSummaryGenerator.SUMMARY_TYPE_CATEGORY_TOPIC);

        Iterator it = categoryTopicsSummary.entrySet().iterator();
        while (it.hasNext()) {
            int currentPlace = 0;
            int count = 0;
            double previousScore = 0;

            Map.Entry pair = (Map.Entry) it.next();

            data += "\n" + pair.getKey() + ":\n===================\n";

            for (ContestApplicationScore score : (ArrayList<ContestApplicationScore>) pair.getValue()) {
                if (previousScore != score.getFinalScore()) {
                    currentPlace++;
                }

                data += currentPlace + ". mjesto:" + score.toString() + "\n";
                previousScore = score.getFinalScore();

                if (++count >= 3) {
                    break;
                }
            }
        }
        return data;
    }
}
