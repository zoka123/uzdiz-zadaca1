package com.zantolov.contest.score;

import com.zantolov.contest.ContestApplicationScore;

import java.util.ArrayList;
import java.util.HashMap;

public class ScoreSummaryGenerator {

    public final static int SUMMARY_TYPE_CATEGORY = 1;
    public final static int SUMMARY_TYPE_CATEGORY_TOPIC = 2;

    private ArrayList<ContestApplicationScore> scores;

    public ArrayList<ContestApplicationScore> getScores() {
        return scores;
    }

    public void setScores(ArrayList<ContestApplicationScore> scores) {
        this.scores = scores;
    }

    public HashMap<String, ArrayList<ContestApplicationScore>> getSummary(int summaryType) {
        switch (summaryType) {
            case ScoreSummaryGenerator.SUMMARY_TYPE_CATEGORY:
                return this.getSummaryByCategory();
            case ScoreSummaryGenerator.SUMMARY_TYPE_CATEGORY_TOPIC:
                return this.getSummaryByTopicInCategory();
        }

        return null;
    }

    private HashMap<String, ArrayList<ContestApplicationScore>> getSummaryByCategory() {
        HashMap<String, ArrayList<ContestApplicationScore>> summary = new HashMap<>();
        for (ContestApplicationScore score : this.scores) {
            String key = score.getApplication().getCamera().getType();
            if (!summary.containsKey(key)) {
                summary.put(key, new ArrayList<ContestApplicationScore>());
            }
            summary.get(key).add(score);
        }

        return summary;
    }

    private HashMap<String, ArrayList<ContestApplicationScore>> getSummaryByTopicInCategory() {
        HashMap<String, ArrayList<ContestApplicationScore>> summary = new HashMap<>();

        for (ContestApplicationScore score : this.scores) {

            String key = score.getApplication().getCamera().getType() + " " + score.getApplication().getTopic();
            if (!summary.containsKey(key)) {
                summary.put(key, new ArrayList<ContestApplicationScore>());
            }
            summary.get(key).add(score);
        }

        return summary;
    }


}
