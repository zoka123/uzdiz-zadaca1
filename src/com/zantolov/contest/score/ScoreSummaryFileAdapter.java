package com.zantolov.contest.score;

import com.zantolov.contest.ContestApplicationScore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ScoreSummaryFileAdapter {

    private ScoreSummaryGenerator scoreSummary;

    public ScoreSummaryFileAdapter(ScoreSummaryGenerator scoreSummary) {
        this.scoreSummary = scoreSummary;
    }

    public ScoreSummaryGenerator getScoreSummary() {
        return scoreSummary;
    }

    public void setScoreSummary(ScoreSummaryGenerator scoreSummary) {
        this.scoreSummary = scoreSummary;
    }

    public String printScoreSummary() {
        String data = "";
        data += this.printTotalScore();
        data += this.printScorePerCategory();
        data += this.printScorePerTopicCategory();
        return data;
    }

    protected String printTotalScore() {
        String result = "";
        result += "TOTAL SCORES:\n=============\n";

        int currentPlace = 0;
        double previousScore = 0;
        for (ContestApplicationScore score : this.getScoreSummary().getScores()) {
            if (score.isDisqualified()) {
                continue;
            }

            if (previousScore != score.getFinalScore()) {
                currentPlace++;
            }

            result += currentPlace + ". mjesto: " + score.toString();
            result += score.getVotesSummary() + "\n";

            previousScore = score.getFinalScore();
        }

        return result;
    }

    protected String printScorePerCategory() {
        String result = "\n\nSCORES BY CATEGORY:\n===================\n";
        HashMap<String, ArrayList<ContestApplicationScore>> categorySummary = this.scoreSummary.getSummary(ScoreSummaryGenerator.SUMMARY_TYPE_CATEGORY);

        Iterator it = categorySummary.entrySet().iterator();
        while (it.hasNext()) {
            int currentPlace = 0;
            double previousScore = 0;

            Map.Entry pair = (Map.Entry) it.next();
            result += "\n" + pair.getKey() + ":\n===================\n";

            for (ContestApplicationScore score : (ArrayList<ContestApplicationScore>) pair.getValue()) {
                if (score.isDisqualified()) {
                    continue;
                }

                if (previousScore != score.getFinalScore()) {
                    currentPlace++;
                }

                result += currentPlace + ". mjesto:" + score.toString();
                result += score.getVotesSummary() + "\n";

                previousScore = score.getFinalScore();
            }
        }

        return result;
    }

    protected String printScorePerTopicCategory() {
        String result = "\n\nSCORES BY TOPIC IN CATEGORY:\n================================\n";

        HashMap<String, ArrayList<ContestApplicationScore>> categoryTopicsSummary = this.scoreSummary.getSummary(ScoreSummaryGenerator.SUMMARY_TYPE_CATEGORY_TOPIC);

        Iterator it = categoryTopicsSummary.entrySet().iterator();
        while (it.hasNext()) {
            int currentPlace = 0;
            double previousScore = 0;

            Map.Entry pair = (Map.Entry) it.next();

            result += "\n" + pair.getKey() + ":\n===================\n";

            boolean disqualifiedLabelPrinted = false;
            for (ContestApplicationScore score : (ArrayList<ContestApplicationScore>) pair.getValue()) {

                if (!disqualifiedLabelPrinted && score.isDisqualified()) {
                    result += "\n -- DISQUALIFIED --  \n";
                    disqualifiedLabelPrinted = true;
                }

                if (previousScore != score.getFinalScore()) {
                    currentPlace++;
                }

                if (!score.isDisqualified()) {
                    result += currentPlace + ". mjesto:";
                }

                result += score.toString();
                result += score.getVotesSummary();
                result += "\n";
                previousScore = score.getFinalScore();
            }
        }

        return result;
    }

}
