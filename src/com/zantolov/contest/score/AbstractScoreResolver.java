package com.zantolov.contest.score;

import com.zantolov.contest.ContestApplicationScore;
import com.zantolov.random.RandomNumberGenerator;

abstract public class AbstractScoreResolver implements ScoreResolverInterface {

    private RandomNumberGenerator randomNumberGenerator;

    public AbstractScoreResolver(RandomNumberGenerator randomNumberGenerator) {
        this.randomNumberGenerator = randomNumberGenerator;
    }

    public RandomNumberGenerator getRandomNumberGenerator() {
        return randomNumberGenerator;
    }

    public void setRandomNumberGenerator(RandomNumberGenerator randomNumberGenerator) {
        this.randomNumberGenerator = randomNumberGenerator;
    }

    public void setScoreCorrectness(ContestApplicationScore score) {
        int correctness = this.getRandomNumberGenerator().getRandomIntBetween(0, 21);
        score.setCorectness(correctness);
        if (score.getCorectness() < 2) {
            score.setDisqualified(true);
        }
    }
}
