package com.zantolov.contest;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class ContestApplicationScoreTest {

    @Test
    public void testCompareTo() throws Exception {

        ArrayList<ContestApplicationScore> applicationScores = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            ContestApplicationScore score = new ContestApplicationScore(new ContestApplication());
            score.setFinalScore(i);
            score.setCorectness(i);

            if (score.getCorectness() < 5) {
                score.setDisqualified(true);
            }

            applicationScores.add(score);
        }

        Collections.shuffle(applicationScores, new Random(System.nanoTime()));
        Collections.sort(applicationScores);

        applicationScores.size();


    }
}