package com.zantolov.contest;

import com.zantolov.camera.CameraInterface;
import com.zantolov.photo.Photo;

public class ContestApplication {

    private int contestant;
    private String topic;
    private CameraInterface camera;
    private Photo photo;

    public int getContestant() {
        return contestant;
    }

    public void setContestant(int contestant) {
        this.contestant = contestant;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public CameraInterface getCamera() {
        return camera;
    }

    public void setCamera(CameraInterface camera) {
        this.camera = camera;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }
}
