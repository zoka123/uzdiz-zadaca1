package com.zantolov.contest.topic;

import com.zantolov.random.RandomGeneratorAwareInterface;
import com.zantolov.random.RandomNumberGenerator;

import java.util.ArrayList;

public class ContestTopicMapBuilder implements RandomGeneratorAwareInterface {

    private RandomNumberGenerator generator;

    public ContestTopicMapBuilder(RandomNumberGenerator generator) {
        this.generator = generator;
    }

    @Override
    public void setGenerator(RandomNumberGenerator generator) {
        this.generator = generator;
    }

    public ArrayList make(int n) {

        if (n > 10 || n < 1) {
            throw new IllegalArgumentException();
        }

        ArrayList<String> topics = new ArrayList<String>();

        topics.add("Nature");
        topics.add("Concert");
        topics.add("Portrait");
        topics.add("Group");
        topics.add("Sport");
        topics.add("Macro");
        topics.add("Wedding");
        topics.add("Aerial");
        topics.add("Underwater");
        topics.add("Abstract");

        while (topics.size() > n) {
            int random = this.generator.getRandomInt();
            topics.remove(random % topics.size());
            topics.trimToSize();
        }

        return topics;
    }
}
