package com.zantolov.contest.topic.test;

import com.zantolov.contest.topic.ContestTopicMapBuilder;
import com.zantolov.random.RandomNumberGenerator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ContestTopicMapBuilderTest {

    @Test
    public void testSetGenerator() throws Exception {
        RandomNumberGenerator g = new RandomNumberGenerator(123);
        ContestTopicMapBuilder builder = new ContestTopicMapBuilder(g);
        for (int i = 1; i <= 10; i++) {
            assertEquals(i, builder.make(i).size());
        }

    }
}