package com.zantolov.contest;

import com.zantolov.contest.score.ScoreResolverInterface;
import com.zantolov.contest.score.ScoreSummaryConsoleAdapter;
import com.zantolov.contest.score.ScoreSummaryFileAdapter;
import com.zantolov.contest.score.ScoreSummaryGenerator;
import com.zantolov.photo.Photo;
import com.zantolov.photo.PhotoProcessFacade;
import com.zantolov.random.RandomNumberGenerator;
import com.zantolov.registry.Registry;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class ContestFacade {

    private static ContestFacade instance;

    private Registry registry;

    private ContestFacade() {
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    public static ContestFacade getInstance() {
        if (instance == null) {
            instance = new ContestFacade();
        }
        return instance;
    }

    public void run() {

        Registry registry = this.getRegistry();
        ArrayList<ContestApplication> applications = this.generateApplications();
        ArrayList<ContestApplicationScore> scores = this.generateScores(applications);

        ScoreSummaryGenerator scoreSummaryGenerator = registry.getScoreSummaryGenerator();
        scoreSummaryGenerator.setScores(scores);

        // Print to console
        ScoreSummaryConsoleAdapter consoleAdapter = new ScoreSummaryConsoleAdapter(scoreSummaryGenerator);
        System.out.print(consoleAdapter.printScoreSummary());

        // Print to file
        ScoreSummaryFileAdapter fileWriter = new ScoreSummaryFileAdapter(scoreSummaryGenerator);
        String fileContent = fileWriter.printScoreSummary();
        try {
            PrintWriter writer = new PrintWriter(registry.getOutputFilename(), "UTF-8");
            writer.print(fileContent);
            writer.close();
        } catch (Exception e) {
            // Supress error
        }
    }

    /**
     * @param applications applications needing scores generation
     * @return array list of scores for given applications
     */
    private ArrayList<ContestApplicationScore> generateScores(ArrayList<ContestApplication> applications) {
        Registry registry = this.getRegistry();

        ArrayList<ContestApplicationScore> applicationScores = new ArrayList<>();
        for (ContestApplication application : applications) {
            ContestApplicationScore score = new ContestApplicationScore(application);

            HashMap<Integer, Integer> votes = new HashMap<>();
            for (int i = 1; i <= registry.getMaxJuryMembersNumber(); i++) {
                votes.put(i, registry.getGenerator().getRandomIntBetween(0, 10, true));
            }
            score.setScores(votes);
            applicationScores.add(score);

            ScoreResolverInterface scoreResolver = registry.getScoreResolver();
            score.setFinalScore(scoreResolver.calculateFinalScore(votes));
            scoreResolver.setScoreCorrectness(score);
        }

        Collections.sort(applicationScores);

        return applicationScores;
    }

    /**
     * @return random generated applications
     */
    private ArrayList<ContestApplication> generateApplications() {

        Registry registry = this.getRegistry();
        RandomNumberGenerator generator = registry.getGenerator();

        int maxNumberOfTopics = generator.getRandomIntBetween(1, registry.getMaxTopicNumber(), true);
        int maxNumberOfCategory = generator.getRandomIntBetween(1, 3, true);
        int maxNumberOfCompetitor = generator.getRandomIntBetween(1, registry.getMaxCompetitorNumber(), true);

        PhotoProcessFacade photoProcessFacade = new PhotoProcessFacade();

        ArrayList topics = registry.getContestTopicMapBuilder().make(generator.getRandomIntBetween(1, maxNumberOfTopics));

        ArrayList<ContestApplication> applications = new ArrayList<>();
        for (int i = 1; i <= maxNumberOfCompetitor; i++) {
            for (int j = 0; j < maxNumberOfCategory; j++) {
                for (Object topic : topics) {
                    // Get some randomness
                    if (0 == (generator.getRandomInt() % 7)) {
                        ContestApplication application = new ContestApplication();
                        application.setContestant(i);
                        application.setTopic((String) topic);
                        application.setCamera(registry.getCameraGenerator().getRandomCamera());

                        Photo photo = new Photo(
                                generator.getRandomDoubleBetween(1, 5),
                                generator.getRandomDoubleBetween(1, 5)
                        );
                        photoProcessFacade.processPhoto(photo);
                        application.setPhoto(photo);
                        applications.add(application);
                    }
                }
            }
        }

        return applications;
    }

    private Registry getRegistry() {
        return registry;
    }

    public void setRegistry(Registry registry) {
        this.registry = registry;
    }

}
