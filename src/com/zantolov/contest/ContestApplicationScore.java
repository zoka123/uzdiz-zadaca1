package com.zantolov.contest;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ContestApplicationScore implements Comparable<ContestApplicationScore> {

    private ContestApplication application;

    private int corectness;

    private HashMap<Integer, Integer> scores;

    private boolean disqualified = false;

    private double finalScore;

    @Override
    public int compareTo(ContestApplicationScore secondScore) {
        return Double.compare(secondScore.getFinalScore(), this.getFinalScore());
    }

    public ContestApplicationScore(ContestApplication application) {
        this.setApplication(application);
    }

    public ContestApplication getApplication() {
        return application;
    }

    public void setApplication(ContestApplication application) {
        this.application = application;
    }

    public int getCorectness() {
        return corectness;
    }

    public void setCorectness(int corectness) {
        this.corectness = corectness;
    }

    public HashMap<Integer, Integer> getScores() {
        return this.scores;
    }

    public void setScores(HashMap<Integer, Integer> scores) {
        this.scores = scores;
    }

    public boolean isDisqualified() {
        return disqualified;
    }

    public void setDisqualified(boolean disqualified) {
        this.disqualified = disqualified;
    }

    public double getFinalScore() {
        if (this.isDisqualified()) {
            return 0;
        }
        return finalScore;
    }

    public void setFinalScore(double finalScore) {
        this.finalScore = finalScore;
    }

    public String getVotesSummary() {
        String output = "";
        output += " \t\tScores: ";
        Iterator it = this.scores.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
//            System.out.println(pair.getKey() + " = " + pair.getValue());
            output += pair.getValue() + "\t ";
        }
        return output;
    }

    public String toString() {

        String output = "";
        output += " Contestant: " + this.getApplication().getContestant();
        output += " \tFinalScore: " + this.getFinalScore();
//        output += " \tDisqualified: " + this.isDisqualified();
        output += " \tCorrectness: " + this.getCorectness();
        output += " \tCamera: " + this.getApplication().getCamera().getName();
        output += " \tTopic: " + this.getApplication().getTopic();

        return output;
    }
}
