package com.zantolov.random;

import java.util.Random;

public class RandomNumberGenerator {

    private Random generator;

    public RandomNumberGenerator(long seed) {
        this.generator = new Random(seed);
    }

    public void setSeed(long seed) {
        this.generator.setSeed(seed);
    }

    public int getRandomInt() {
        return this.generator.nextInt(9999999);
    }

    public int getRandomIntBetween(int bottom, int up, boolean includeEdges) {
        if (includeEdges) {
            up = up + 1;
            bottom = bottom - 1;
        }
        return this.getRandomIntBetween(bottom, up);

    }

    public int getRandomIntBetween(int bottom, int up) {
        if (bottom == up) {
            throw new IllegalArgumentException();
        }

        up = up - 1;
        bottom = bottom + 1;

        return this.generator.nextInt(up - bottom + 1) + bottom;
    }


    public double getRandomDoubleBetween(int bottom, int up) {
        bottom = bottom * 1000;
        up = up * 1000;
        int random = this.getRandomIntBetween(bottom, up);
        return ((double) random) / 1000;
    }
}
