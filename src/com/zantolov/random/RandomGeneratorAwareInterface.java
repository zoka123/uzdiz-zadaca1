package com.zantolov.random;

public interface RandomGeneratorAwareInterface {

    public void setGenerator(RandomNumberGenerator generator);

}
