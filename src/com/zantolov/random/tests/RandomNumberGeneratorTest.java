package com.zantolov.random.tests;

import com.zantolov.random.RandomNumberGenerator;
import org.junit.Test;

import static org.junit.Assert.*;

public class RandomNumberGeneratorTest {

    @Test
    public void testGetRandomIntBetween() throws Exception {
        RandomNumberGenerator generator = new RandomNumberGenerator(123);

        int bottom = 5;
        int up = 10;
        for (int i = 0; i < 100; i++) {
            int random = generator.getRandomIntBetween(bottom, up);
            System.out.print(random);
            assertTrue(bottom < random);
            assertTrue(up > random);
        }
        System.out.print("\n");
    }

    @Test
    public void testGetRandomIntBetweenWithEdges() throws Exception {
        RandomNumberGenerator generator = new RandomNumberGenerator(123);

        int bottom = 5;
        int up = 8;
        for (int i = 0; i < 100; i++) {
            int random = generator.getRandomIntBetween(bottom, up, true);
            System.out.print(random);
            assertTrue(bottom <= random);
            assertTrue(up >= random);
        }
    }
}